+++
title = 'Über'
date = 2023-10-11T16:56:28+02:00
draft = false
heroStyle = 'background'
+++

**nur flossen** (german for only fins) is a private project.

## Thanks

Many thanks to [Heatherhorns](https://heaterhorns.com/emoji) for letting me use the Blobhajar on the site

-- Blobhaj by [Heatherhorns](https://heaterhorns.com/) is licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/).

## How do I get a page for a Haj?

### via a merge request

*Instructions coming soon*

### Manually

You take a picture of your Blåhaj. You e-mail the picture to me at haj /a\ nurflossen (.) de, with the following information:

- Name of the haj
- A short text describing the picture of the haj
- Realm / habitat *optional*
- Marking (fin bands or something) *optional*
- Pronouns *optional*
- Things your Haj loves *optional*
- Things your Haj hates *optional*
- Friends (a list of friendly plushies) *optional*

You are also welcome to write some text about your Haj, which will be added to the page.
