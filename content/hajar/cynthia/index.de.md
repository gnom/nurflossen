+++
title = 'Cynthia'
realm = "Auf meinem smolhaj.social und im DB Fernverkehr"
marking = "\"Cynthia\" und \"nwex.de\" auf dem Textilpflegehinweis"
pronouns = "sie/ihr"
loves = "Kekfe nompfen, smolhaj.social und alles vollkrümeln"
hates = "[BR412](https://bahn.expert/WRSheets/412.13.pdf)"
alt = "Cynthia in einem ICE und mit einem Tütchen entkoffeinierten Instant-Kaffee im Mund."
friends = """
- [Husky](https://smolhaj.social/@husky)
"""
+++

Auf meinem smolhaj.social bin ich natürlich auch <a rel="me" href="https://smolhaj.social/@cynthia">@cynthia@smolhaj.social</a>

<a href="https://webring.noms.ing/previous?from=nurflossen.de/hajar/cynthia">&lt; &lt; &lt;</a> Ich bin auch im [Nomsring](https://webring.noms.ing)! <a href="https://webring.noms.ing/next?from=nurflossen.de/hajar/cynthia">&gt; &gt; &gt;</a>

## Bilder

{{< gallery >}}
    <img src="feature_cynthia.jpg" class="grid-w33" alt="Cynthia in einem ICE und mit einem Tütchen entkoffeinierten Instant-Kaffee im Mund."/>
    <img src="cynthia_001.jpg" class="grid-w33" alt="Cynthia schaut in einem IKEA sehr neidvoll auf einen Korb mit verpackten Kafferep Keksen."/>
    <img src="cynthia_002.jpg" class="grid-w33" alt="Cynthia im Ki Ba La im DB Museum Nürnberg. Hinter ihr sind der kleine ICE und weitere Kinderzüge abgedruckt, darüber der Schriftzug Der kleine ICE und seine Freunde."/>
    <img src="cynthia_003.jpg" class="grid-w33" alt="Cynthia vor einem anderen Smolhaj und einem Blahaj."/>
{{< /gallery >}}
