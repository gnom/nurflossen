+++
title = 'Cynthia'
realm = "On my smolhaj.social and DB Long-Distance Traffic"
marking = '"Cynthia" and "nwex.de" on the Textile Care Instructions'
pronouns = "she/her"
loves = "Nomsing cookies, smolhaj.social and leaving crumbles everywhere"
hates = "[BR412](https://bahn.expert/WRSheets/412.13.pdf)"
alt = 'Cynthia in an ICE train with a decaffinated "coffee stick" in her mouth'
friends = """
- [Husky](https://smolhaj.social/@husky)
"""
+++

On my smolhaj.social I'm also <a rel="me" href="https://smolhaj.social/@cynthia">@cynthia@smolhaj.social</a>

<a href="https://webring.noms.ing/previous?from=nurflossen.de/hajar/cynthia">&lt; &lt; &lt;</a> I'm also in the [Nomsring](https://webring.noms.ing)! <a href="https://webring.noms.ing/next?from=nurflossen.de/hajar/cynthia">&gt; &gt; &gt;</a>

## Images

{{< gallery >}}
    <img src="feature_cynthia.jpg" class="grid-w33" alt='Cynthia in an ICE train with a decaffinated "coffee stick" in her mouth.'/>
    <img src="cynthia_001.jpg" class="grid-w33" alt="Cynthia in an IKEA, looking onto a basket filled with rolls of Kafferep cookies with jealousy."/>
    <img src="cynthia_002.jpg" class="grid-w33" alt='Cynthia in the "Ki Ba La" in the DB Museum Nuremberg. Behind her are drawings of the small ICE and other childen trains, above that a text saying "The small ICE and his friends".'/>
    <img src="cynthia_003.jpg" class="grid-w33" alt="Cynthia in front of another Smolhaj and a Blahaj."/>
{{< /gallery >}}
