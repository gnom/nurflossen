+++
title = 'Hajland'
realm = "lives with [gim](https://queers.group/@gim)"
pronouns = "corpus / him"
loves = "everyone like his neighbour"
hates = "this chalice, his father"
alt = "The hajland lies on a blanket and stretches his head upwards. A television can be seen in the background."
friends = """
- [#JudasTheCat](https://queer.group/tags/judasthecat)
- [Oktavia the I](https://oktavia.gim.gay/cephalopods/oktavia-i/)
- Hank Seven
"""
+++
